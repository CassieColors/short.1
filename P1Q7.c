#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main (int argc, char *argv[]) {
	pid_t childpid = 0;
	int c, i, n, j;
	int hflag = 0;
	char* numberCheck;
	char errorString[50];

	// Check for any options passed with the executable and respond accordingly	
	while ((c = getopt (argc, argv, "h")) != -1){
		switch (c){
			case 'h':
				hflag = 1;
				break;
			case '?':
				// If the flag wasn't recognized, print a list of available options
        			if (isprint (optopt))
					// If the flag passed was a number, the user was probably trying to pass a negative process number
					if(isdigit(optopt)){
						errno = EINVAL;
						sprintf(errorString, "%s: Error: Negative argument", argv[0]);
						perror(errorString);
						fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);

          				} else {
						fprintf (stdout, "Available options -- \'h\'.\n", optopt);
              				}
				return 1;
			default:
				abort();
		}
	}
	// If the hflag is detected, print help message and exit
	// If there was no second argument or there were too many arguments, print an appropriate error message and exit
	if(hflag){
		fprintf(stdout, "First argument: %s\nSecond Argument: A positive integer for desired number of processes\n", argv[0]);
		return 0;
	} 
	else if(argc < 2){
		errno = EINVAL;
		sprintf(errorString, "%s: Error: Too few arguments", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	} else if(argc > 2){		
                errno = E2BIG;
             	sprintf(errorString, "%s: Error", argv[0]);
		perror(errorString);
                fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
                return 1;
	} else {
		numberCheck = argv[1];
	}

	n = atoi(argv[1]);
	if(n == 0){
		errno = EINVAL;
		sprintf(errorString, "%s: Error: Cannot start 0 processes", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	}
	for(i = 1; i < n; i++){

		if(childpid = fork()){
			if(errno != 0){
				sprintf(errorString, "%s: Error: Forking error", argv[0]);
				perror(errorString);
			}
			break;
		}
	}
	fprintf(stdout, "i:%d ", i);
	fprintf(stdout, "PID: %ld ", (long)getpid());
	fprintf(stdout, "PPID: %ld ", (long)getppid());
	fprintf(stdout, "child PID: %ld\n", (long)childpid);
	return 0;
}
