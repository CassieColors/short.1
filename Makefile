EXECS = P1Q123 P1Q4 P1Q5 P1Q6 P1Q7 P1Q8

all: $(EXECS)

P1Q123: P1Q123.c
	gcc -o P1Q123 P1Q123.c

P1Q4: P1Q4.c
	gcc -o P1Q4 P1Q4.c

P1Q5: P1Q5.c
	gcc -o P1Q5 P1Q5.c

P1Q6: P1Q6.c
	gcc -o P1Q6 P1Q6.c

P1Q7: P1Q7.c
	gcc -o P1Q7 P1Q7.c

P1Q8: P1Q8.c
	gcc -o P1Q8 P1Q8.c

clean:
	rm -f *.o
	rm -f $(EXECS)
