Cassandra Short
CS4760
Answers Project 1 (Exercise 3.8)

General Observation: For all of the programs in this project, I did notice that sometimes the program, when finished,
	wouldn't exit all the way back to the bash shell, but instead hung until you sent an interrupt (^c).
	Commands can be executed without interrupting, and seem to complete normally, but I thought it was worth noting.
	This behavior seemed to happen more often with larger numbers of processes. On the other hand (as seen
        in some of the example outputs below) the programs sometimes exited to bash mid-run and continued as well.


1. Example Output
   n = 10
	-bash-4.2$ P1Q1 10
	i:1 process ID:12781 parent ID:11164 child ID:12782
	i:2 process ID:12782 parent ID:12781 child ID:12783
	i:3 process ID:12783 parent ID:1 child ID:12784
	-bash-4.2$ i:4 process ID:12784 parent ID:1 child ID:12785
	i:5 process ID:12785 parent ID:12784 child ID:12786
	i:6 process ID:12786 parent ID:12785 child ID:12787
	i:7 process ID:12787 parent ID:1 child ID:12788
	i:8 process ID:12788 parent ID:12787 child ID:12789
	i:9 process ID:12789 parent ID:1 child ID:12790
	i:10 process ID:12790 parent ID:12789 child ID:0
	
   Observation
	The program uses the second argument as the number of processes to start. For every process,
	a line is printed listing the process number, process ID, process parent ID, and child process ID.
	After testing the program with between 1 and 20 processes, I first noticed that if the parent
	terminated before it's child, the parent ID for the child becomes 1, meaning the parent is now 'init'.
	The final process's child ID is always 0 because it's the last child and fork() returns 0 to the child.
	I also noticed that the processes don't necessarily finish or print in order, as sometimes the 'i' value, 
	or the order the processes are generated in the for loop, is not always sorted.

2. Example Output
   n = 4
	-bash-4.2$ P1Q1 4
	i:1 process ID:13325 parent ID:11164 child ID:13326
	i:2 process ID:13326 parent ID:13325 child ID:13327
	i:3 process ID:13327 parent ID:1 child ID:13328
	-bash-4.2$ i:4 process ID:13328 parent ID:1 child ID:0
   
   Observation
	Process 1: 13325
	Process 2: 13326
	Process 3: 13327
	Process 4: 13328

3. 	On my machine, I ran the command 'P1Q3 1000000' thirty or forty times. The highest number of processes I was 
	able to create without an error was 101756 processes and the earliest error occurred at 585 processes.
	Every time the program failed, the error message delivered by perror was "Resource temporarily unavailable".
	When there's an error, the child PID is always -1 because fork() returns -1 when it fails to create a new
	process. As far as processes being adopted by init, I observed anywhere between 20 and 50 percent of the total
	processes having a PPID of 1, with the proportion never actually reaching 50 percent. On average, it seemed that
	around 30 percent of the processes were orphaned and adopted by init.

4.	The maximum number of processes I was able to generate with with sleep(10) before the final print
   	statement was 28.

5. Example Output
	i:1 process ID:7094 parent ID:4005 child ID:7095
	i:2 process ID:7095 parent ID:7094 child ID:7096
	i:3 process ID:7096 parent ID:7095 child ID:7097
	i:4 process ID:7097 parent ID:7096 child ID:7098
	i:5 process ID:7098 parent ID:7097 child ID:0
	i:1 process ID:7094 parent ID:4005 child ID:7095
	i:2 process ID:7095 parent ID:7094 child ID:7096
	i:3 process ID:7096 parent ID:7095 child ID:7097
	i:5 process ID:7098 parent ID:7097 child ID:0
	i:4 process ID:7097 parent ID:7096 child ID:7098
	i:1 process ID:7094 parent ID:4005 child ID:7095
	i:2 process ID:7095 parent ID:7094 child ID:7096
	i:3 process ID:7096 parent ID:7095 child ID:7097
	i:4 process ID:7097 parent ID:7096 child ID:7098
	i:5 process ID:7098 parent ID:7097 child ID:0
	
   Observation
	With the final print statement and sleep(m) inside of a loop that is iterated k times, the program still creates n
	processes, but now it reprints these same processes k times with m seconds between each group of printed processes.

6. Example Output
	i:5 process ID:7876 parent ID:7875 child ID:0
	i:4 process ID:7875 parent ID:7874 child ID:7876
	i:3 process ID:7874 parent ID:7873 child ID:7875
	i:2 process ID:7873 parent ID:7872 child ID:7874
	i:1 process ID:7872 parent ID:4005 child ID:7873
	
   Observation
	With wait(), just before the final print statement, the order that the processes are printed is reversed, such
	that the process created by i = 1 in the for loop prints last and the process created by i = n is printed first.
	This implies that each parent waits for it's children to be done before finishing.

7. Example Output
	P1Q7 7
	i:1 PID: 19597 PPID: 18685 child PID: 19598
	i:2 PID: 19598 PPID: 1 child PID: 19599
	-bash-4.2$ i:3 PID: 19599 PPID: 19598 child PID: 19600
	i:4 PID: 19600 PPID: 1 child PID: 19601
	i:5 PID: 19601 PPID: 1 child PID: 19602
	i:6 PID: 19602 PPID: 1 child PID: 19603
	i:7 PID: 19603 PPID: 19602 child PID: 0

   Observation
	When I split the final print statement into four separate statements, I got a segmentation fault until I removed
	the error checking I had added that looped through the command line argument for n to check that all of its digits
	were numeric. After removing this, the program still prints the index value, PID, PPID, and child PID, with each
	process having its own line. It doesn't appear that any of the lines were scrambled, or that the PID's got mixed
	between lines, but there was a much larger ratio of orphaned processes, whose parent PIDs were 1. It's still clear
	which processes belong to which index of the for loop, as all of the PIDs were still in order all of the times
	that I ran it.

8. Example Output
	(hitting return to print rest of lines)
	-bash-4.2$ P1Q8 4 5
	29543: dafas
	-bash-4.2$ 29544:
	29545:
	29546:

	(hitting another key to print rest of lines)
	-bash-4.2$ P1Q8 4 5
	adfadf
	29628: adfad
	-bash-4.2$ 29629: d
	29630: d
	29631: d
	
   Observation
	I chose to use scanf to collect user input, which allows them to enter any number of characters until they hit return
	but it will only save the number of characters the user specified on the command line. I set it to ignore any leading
	whitespace or newlines. I observed that these characters saved to the buffer will only print in the fprintf associated
	with the first process, while the buffers of the other processes remain empty. You must hit a key to print all of the
	subsequent processes, so it seems that scanf at least starts in each process, but immediately terminates in all but
	the first process. If you enter 0 as the number of characters to collect, it prints all processes immediately with 
	nothing in the buffer.
