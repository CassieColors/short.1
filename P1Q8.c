#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main (int argc, char *argv[]) {
	pid_t childpid = 0;
	int c, i, n, j;
	int q = 0;
	int hflag = 0;
	char* numberCheck;
	char errorString[50];
	int nchars = 0;

	// Check for any options passed with the executable and respond accordingly	
	while ((c = getopt (argc, argv, "h")) != -1){
		switch (c){
			case 'h':
				hflag = 1;
				break;
			case '?':
				// If the flag wasn't recognized, print a list of available options
        			if (isprint (optopt))
					// If the flag passed was a number, the user was probably trying to pass a negative process number
					if(isdigit(optopt)){
						errno = EINVAL;
						sprintf(errorString, "%s: Error: Negative argument", argv[0]);
						perror(errorString);
						fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);

          				} else {
						fprintf (stdout, "Available options -- \'h\'.\n", optopt);
              				}
				return 1;
			default:
				abort();
		}
	}
	// If the hflag is detected, print help message and exit
	// If there were not enough or too many arguments, print an appropriate error message and exit
	if(hflag){
		fprintf(stdout, "First argument: %s\nSecond Argument: A positive integer for desired number of processes\n", argv[0]);
		fprintf(stdout, "Third argument: A positive integer for desired number of characters to read from stdin\n");
		return 0;
	} 
	else if(argc < 3){
		errno = EINVAL;
		sprintf(errorString, "%s: Error: Too few arguments", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	} else if(argc > 3){		
                errno = E2BIG;
             	sprintf(errorString, "%s: Error", argv[0]);
		perror(errorString);
                fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
                return 1;
	} else {
		numberCheck = argv[1];
	}

	// Check to make sure that the argument passed for n is an integer
	for(; numberCheck[j] != 0; j++){
		if(numberCheck[j] > '9' || numberCheck[j] < '0'){
			if (!isdigit(numberCheck[j])){
				errno = EINVAL;
				sprintf(errorString, "%s: Error: Non-integer argument", argv[0]);
				perror(errorString);
				fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
				return 1;
			}
                }
	}
	n = atoi(argv[1]);
	nchars = atoi(argv[2]);

	// Create dynamic array of the size specified by the user to hold their input
	char* mybuf = malloc((nchars + 1) * sizeof(char));
	if(n == 0){
		errno = EINVAL;
		sprintf(errorString, "%s: Error: Cannot start 0 processes", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	}
	for(i = 1; i < n; i++){

		if(childpid = fork()){
			if(errno != 0){
				sprintf(errorString, "%s: Error: Forking error", argv[0]);
				perror(errorString);
			}
			break;
		}
	}
	// Gather the specified number of characters. If the user enters more, they will be ignored
	for(; q < nchars; q++){
		scanf(" %c", &mybuf[q]);
	}
	mybuf[nchars] = '\0';
	fprintf(stdout, "%ld: %s\n", (long)getpid(), mybuf);
	free(mybuf);
	return 0;
}
