SYNOPSIS 

	These programs are implementations for Exercise 3.8, which has 8 problems, all of which observe processes. 
	Because the first 3 problems didn't require code changes, I combined them all into 1 program that included 
	my perror, flag, and getopt additions. For the remaining 5 problems I decided to keep them in separate executables
	so that each problem's output could still be tested independently, without the changes interfering with one another.
	This was especially helpful for programs which used different numbers of executables, to keep -h output accurate.
	Each program is named for it's project number and question number; this is project 1, so the 5th problem is P1Q5.
	The only exception to this is the first program, which covers problems 1, 2, & 3 and is named P1Q123.

	These programs all take an integer command line argument and call fork() as many times as that argument demands,
        creating a child process each time. Some of them also take additional arguments. Before each process terminates,
        it prints the current index within the for loop of the original process, its own process ID, it's parent's
        process ID, and it's child's process ID. In P1Q8 this output does change to include a buffer string, and removes
	all ID's except the current PID.


EXAMPLE CALL

	'P1Q123 10' : calls the P1Q123 executable and orders it to create 10 processes
	General Command: '{EXE} {integer}'

HELP
	For a list of the arguments that can be passed to each of these programs, use command '{EXE} -h'
	For example: P1Q123 -h

SETUP

	These programs include a single Makefile, which builds executables (named without extensions) for
	all 6 programs. Bitbucket is used for version control, and has been attached using an SSH key.
	Use command 'git log' to view a history of all commits and pushes from the project's home directory
	'short.1' in MY home directory. I've also included a file that contains a copy of my final git log, for viewing
	outside of my home directory.

SPECIAL NOTE
	
	For program P1Q8, which collects a number of characters specified by the user on the command line, the user can
	enter any number of characters, but only the number they specified will be saved to the buffer. The user must
	hit enter to stop gathering input and continue to the rest of the program.
